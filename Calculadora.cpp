/*
Senai Dendezeiros
Aluno: SEU NOME
Data: 06/06/2019
Calculadora em C++ Orientada à Objetos
*/
#include <iostream>
#include "calculadora.h"
using namespace std;
calculadora::calculadora(){
	cout<<("Calculadora criada")<<endl;
};
calculadora::~calculadora(){
	cout<<("Calculadora destruída")<<endl;
};
float calculadora::getResultado(){
	cout<<(this->resultado)<<endl;
	return (this->resultado);
};
void calculadora::setResultado(float result){
	this->resultado=result;
};
float calculadora::somar(float x, float y){
	cout<<x<<" + "<<y<<" = ";
	this->setResultado(x+y);
	return (this->getResultado());
};
float calculadora::subtrair(float x, float y){
	cout<<x<<" - "<<y<<" = ";
	this->setResultado(x-y);
	return (this->getResultado());
};
float calculadora::multiplicar(float x, float y){
	cout<<x<<" * "<<y<<" = ";
	this->setResultado(x*y);
	return (this->getResultado());
};
float calculadora::dividir(float x, float y){
	cout<<x<<" / "<<y<<" = ";
	this->setResultado(x/y);
	return (this->getResultado());
};

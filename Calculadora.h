#ifndef CALCULADORA_H
#define CALCULADORA_H
class calculadora
{
private:
	float resultado;
public:
	calculadora();
	~calculadora();
	float getResultado();
	void setResultado(float result);
	float somar(float x, float y);
	float subtrair(float x, float y);
	float multiplicar(float x, float y);
	float dividir(float x, float y);
};
#endif

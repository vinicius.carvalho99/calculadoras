<?php
class calculadora{
    private $result;
    public function getResult(){
        return $this->result;
    }
    public function setResult($res){
        $this->result = $res;
    }
    public function somar($x, $y){
        $this->setResult($x, $y);
        echo("$x + $y = ".$this->getResult()."\n");
        return ($this->getResult());
    }
    public function subtrair($x, $y){
        $this->setResult($x, $y);
        echo("$x - $y = ".$this->getResult()."\n");
        return ($this->getResult());
    }
    public function multiplicar($x, $y){
        $this->setResult($x, $y);
        echo("$x * $y = ".$this->getResult()."\n");
        return ($this->getResult());
    }
    public function dividir($x, $y){
        $this->setResult($x, $y);
        echo("$x / $y = ".$this->getResult()."\n");
        return ($this->getResult());
    }
}    
$calc= new calculadora();
$calc->somar(2,2);
?>